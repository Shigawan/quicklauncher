﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
  public class SeparatorConfigurationEntity : IMenuItemConfigurationEntity
  {
    public SeparatorConfigurationEntity() { }

    public System.Windows.Forms.MenuItem GetConfigurationMenuItem()
    {
      var Item = new LauncherMenuItem(true);
      
      return Item;
    }
  }
}
