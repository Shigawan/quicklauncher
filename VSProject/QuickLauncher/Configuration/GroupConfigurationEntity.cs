﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
  [System.Xml.Serialization.XmlRoot("Group")]
  public class GroupConfigurationEntity : BaseMenuItemConfigurationEntity, IMenuItemConfigurationEntity
  {
    [System.Xml.Serialization.XmlElementAttribute("Launcher", typeof(LauncherConfigurationEntity))]
    [System.Xml.Serialization.XmlElementAttribute("Group", typeof(GroupConfigurationEntity))]
    [System.Xml.Serialization.XmlElementAttribute("Separator", typeof(SeparatorConfigurationEntity))]
    [System.Xml.Serialization.XmlElementAttribute("WebLauncher", typeof(WebLauncherConfigurationEntity))]
    public object[] MenuItems { get; set; }

    public GroupConfigurationEntity()
      : base()
    {
      Title = "Group";
    }

    public System.Windows.Forms.MenuItem GetConfigurationMenuItem()
    {
      var convertedMenuItems = new System.Windows.Forms.MenuItem[MenuItems.Length];
      for (int i = 0; i < MenuItems.Length; i++)
      {
        convertedMenuItems[i] = ((IMenuItemConfigurationEntity)MenuItems[i]).GetConfigurationMenuItem();
      }

      var Item = new LauncherMenuItem(Title, convertedMenuItems, IsBold());

      return Item;
    }
  }
}
