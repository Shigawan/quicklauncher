﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
  [System.Xml.Serialization.XmlRoot("WebLauncher")]
  public class WebLauncherConfigurationEntity : BaseMenuItemConfigurationEntity, IMenuItemConfigurationEntity
  {
    [System.Xml.Serialization.XmlAttribute]
    public string URL { get; set; }

    public WebLauncherConfigurationEntity()
      : base()
    {
      Title = "";
      URL = "";
    }

    public System.Windows.Forms.MenuItem GetConfigurationMenuItem()
    {
      var Item = new LauncherMenuItem(Title,
          delegate (object sender, EventArgs e)
          {
            System.Diagnostics.Process.Start(URL);
          }, IsBold());
      
      return Item;
    }
  }
}
