﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace QuickLauncher.Configuration
{
  public class LauncherMenuItem : MenuItem
  {
    private bool isBold = false;
    private bool isSeparator = false;
    
    private Font _Font = SystemFonts.MenuFont;
    private Brush _Brush = SystemBrushes.MenuText;
    private Brush _HighlightBrush = SystemBrushes.HighlightText;

    public LauncherMenuItem(bool isSeparator_IN)
      : base("-")
    {
      isBold = false;
      isSeparator = isSeparator_IN;
      StandardSetup();
    }

    public LauncherMenuItem(string displayText_IN, bool isBold_IN = false)
      : base(displayText_IN)
    {
      isBold = isBold_IN;
      StandardSetup();
    }

    public LauncherMenuItem(string displayText_IN, EventHandler onClick_IN, bool isBold_IN = false) 
      : base(displayText_IN, onClick_IN)
    {
      isBold = isBold_IN;
      StandardSetup();
    }

    public LauncherMenuItem(string displayText_IN, MenuItem[] items_IN, bool isBold_IN = false)
      : base(displayText_IN, items_IN)
    {
      isBold = isBold_IN;
      StandardSetup();
    }

    private void StandardSetup()
    {
      OwnerDraw = true;
      DrawItem += DrawCustomMenuItem;
      MeasureItem += MeasureCustomMenuItem;

      if(isBold)
      {
        _Font = new Font(_Font, FontStyle.Bold);
      }
    }

    public bool Bold { get { return isBold; } set { isBold = value; } }
    public bool Separator { get { return isSeparator; } set { isSeparator = value; } }

    private void DrawCustomMenuItem(object sender, DrawItemEventArgs e)
    {
      LauncherMenuItem senderMenuItem = (LauncherMenuItem)sender;
      Brush drawBrush = _Brush;
      Font drawFont = _Font;
      bool displayText = true;

      if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
      {
        drawBrush = _HighlightBrush;
      }

      e.DrawBackground();

      if(IsParent)
      {
        //e.Graphics.DrawIcon(new Icon(""), )
      }

      if(isSeparator)
      {
        e.Graphics.DrawLine(new Pen(SystemColors.MenuText, 2), 
          e.Bounds.X + 5, 
          e.Bounds.Y + (e.Bounds.Height / 2), 
          e.Bounds.X + e.Bounds.Width - 12, 
          e.Bounds.Y + (e.Bounds.Height / 2));
        displayText = false;
      }

      if (displayText)
      {
        e.Graphics.DrawString(senderMenuItem.Text, drawFont, drawBrush, e.Bounds.X, e.Bounds.Y);
      }
    }

    private void MeasureCustomMenuItem(object sender, MeasureItemEventArgs e)
    {
      LauncherMenuItem customItem = (LauncherMenuItem)sender;

      var StringSize = e.Graphics.MeasureString(customItem.Text, _Font);
      e.ItemWidth = (int)StringSize.Width;
      e.ItemHeight = (int)StringSize.Height;
    }
  }
}
