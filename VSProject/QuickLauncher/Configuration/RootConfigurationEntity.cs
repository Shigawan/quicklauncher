﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
    [System.Xml.Serialization.XmlRoot("Configuration")]
    public class RootConfigurationEntity
    {
        [System.Xml.Serialization.XmlElementAttribute("Launcher", typeof(LauncherConfigurationEntity))]
        [System.Xml.Serialization.XmlElementAttribute("Group", typeof(GroupConfigurationEntity))]
        [System.Xml.Serialization.XmlElementAttribute("Separator", typeof(SeparatorConfigurationEntity))]
        [System.Xml.Serialization.XmlElementAttribute("WebLauncher", typeof(WebLauncherConfigurationEntity))]
        public object[] ConfigurationItems { get; set; }

        public RootConfigurationEntity()
        {

        }
    }
}
