﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
    public interface IMenuItemConfigurationEntity
    {
        System.Windows.Forms.MenuItem GetConfigurationMenuItem();
    }
}
