﻿using System;
using System.Xml.Serialization;
using System.IO;

namespace QuickLauncher.Configuration
{
  public class QuickLauncherConfigurationWrapper
  {
    private RootConfigurationEntity Configuration;
    private string ConfigurationFileLocation;

    public QuickLauncherConfigurationWrapper(string NewConfigurationFileLocation)
    {
      ConfigurationFileLocation = NewConfigurationFileLocation;
      LoadConfiguration();
    }

    public System.Windows.Forms.ContextMenu GetConfiguredContextMenu()
    {
      var returnContextMenu = new System.Windows.Forms.ContextMenu();

      foreach (IMenuItemConfigurationEntity menuEntity in Configuration.ConfigurationItems)
      {
        returnContextMenu.MenuItems.Add(menuEntity.GetConfigurationMenuItem());
      }

      var ExitItem = new LauncherMenuItem("Exit",
                delegate (object sender, EventArgs e)
                {
                  System.Windows.Application.Current.Shutdown();
                });
      
      returnContextMenu.MenuItems.Add(ExitItem);

      return returnContextMenu;
    }

    public void LoadConfiguration()
    {
      LoadConfiguration(ConfigurationFileLocation);
    }

    public void LoadConfiguration(string NewConfigurationFileLocation)
    {
      XmlSerializer serializer = new XmlSerializer(typeof(RootConfigurationEntity));
      using (StreamReader reader = new StreamReader(NewConfigurationFileLocation))
      {
        Configuration = (RootConfigurationEntity)serializer.Deserialize(reader);
      }
    }

    public void SaveConfiguration()
    {
      SaveConfiguration(ConfigurationFileLocation);
    }

    public void SaveConfiguration(string NewConfigurationFileLocation)
    {
      XmlSerializer serializer = new XmlSerializer(typeof(RootConfigurationEntity));
      using (StreamWriter writer = new StreamWriter(NewConfigurationFileLocation))
      {
        serializer.Serialize(writer, Configuration);
      }
    }
  }
}
