﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
  [System.Xml.Serialization.XmlRoot("Launcher")]
  public class LauncherConfigurationEntity : BaseMenuItemConfigurationEntity, IMenuItemConfigurationEntity
  {
    [System.Xml.Serialization.XmlAttribute]
    public string LaunchPath { get; set; }

    [System.Xml.Serialization.XmlAttribute]
    public string LaunchArguments { get; set; }

    public LauncherConfigurationEntity()
      : base()
    {
      Title = "Launcher";
      LaunchPath = "";
      LaunchArguments = "";
    }

    public System.Windows.Forms.MenuItem GetConfigurationMenuItem()
    {
      System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
      startInfo.FileName = LaunchPath;

      if(!string.IsNullOrEmpty(LaunchArguments))
      {
        startInfo.Arguments = LaunchArguments;
      }

      var Item = new LauncherMenuItem(Title,
          delegate (object sender, EventArgs e)
          {
            System.Diagnostics.Process.Start(startInfo);
          }, IsBold());

      return Item;
    }
  }
}
