﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickLauncher.Configuration
{
  public class BaseMenuItemConfigurationEntity
  {
    [System.Xml.Serialization.XmlAttribute]
    public string Title { get; set; }

    [System.Xml.Serialization.XmlAttribute]
    public string Bold { get; set; }

    public BaseMenuItemConfigurationEntity()
    {
      Title = "";
      Bold = "false";
    }

    public bool IsBold()
    {
      return Bold.ToLower() == "true";
    }
  }
}
