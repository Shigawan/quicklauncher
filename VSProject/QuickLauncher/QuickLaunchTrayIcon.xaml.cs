﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace QuickLauncher
{
  public partial class QuickLaunchTrayIcon : Window
  {
    private System.Windows.Forms.NotifyIcon trayIcon;
    private Dictionary<LaunchMode, System.Drawing.Icon> iconImages;

    private enum LaunchMode { Local = 0, Remote = 1 }
    private int CurrentLaunchMode = 0;

    public QuickLaunchTrayIcon()
    {
      InitializeComponent();
    }

    protected override void OnInitialized(EventArgs e)
    {
      iconImages = new Dictionary<LaunchMode, System.Drawing.Icon>();
      iconImages.Add(LaunchMode.Local, new System.Drawing.Icon(System.IO.Path.Combine(Environment.CurrentDirectory, @"Images\Icons\temp_default.ico")));
      iconImages.Add(LaunchMode.Remote, new System.Drawing.Icon(System.IO.Path.Combine(Environment.CurrentDirectory, @"Images\Icons\temp_update.ico")));

      trayIcon = new System.Windows.Forms.NotifyIcon();
      trayIcon.DoubleClick += new EventHandler(Switch_Modes);
      trayIcon.Icon = iconImages[LaunchMode.Local];

      var Wrapper = new Configuration.QuickLauncherConfigurationWrapper(System.IO.Path.Combine(Environment.CurrentDirectory, @"Configuration/Configuration.txt"));

      trayIcon.ContextMenu = Wrapper.GetConfiguredContextMenu();

      base.OnInitialized(e);
    }

    private void OnLoaded(object sender, EventArgs e)
    {
      trayIcon.Visible = true;
    }

    private void Switch_Modes(object sender, EventArgs e)
    {
      CurrentLaunchMode++;
      CurrentLaunchMode = CurrentLaunchMode % 2;

      trayIcon.Icon = iconImages[(LaunchMode)CurrentLaunchMode];
    }
  }
}
